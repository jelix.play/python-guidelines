# Python Guidelines

![Обложка](https://sun9-45.userapi.com/impg/Vo0d40jGan9pY20xTEOspV84TnbznOc7p6UaQA/nGl38u9eKVE.jpg?size=460x657&quality=96&sign=7d3fae2fd29fd9f9c767fa35b618532d&type=album)

## Введение

Тут я планирую выкладывать методические указания для учеников IT-Cube
по программе Python. Если у меня хватит сил, к концу 2022 учебного года
тут должен появиться полный курс. Ничего не обещаю заранее. Но постараюсь
рассмотреть все необходимые темы в том или ином объеме. 

Приветствуется любая помощь: можете написать свою статью, исправить ошибку
в моей и так далее и тому подобное...

## Index

[Основы](./course/basics)

[Функции](./course/functions)

[ООП](./course/oop)

[Структуры данных (контейнеры)](./course/containers)

[Итераторы](./course/iterators.md)

[Исключения](./course/exceptions.md)

[Менеджеры контекста](./course/context_managers.md)

[Работа с файлами](./course/files.md)

[Продвинутые встроенные возможности](./course/builtins_advanced.md)

[Модули и импорты](./course/modules_and_imports.md)

[Стандартная библиотека (TBD)](./course/stl)

## TODO

- Обзор стандартной библиотеки
- git
- Краткий курс по алгоритмам и структурам данных (или введение в него)
- Краткий курс по Flask'у
- Краткий курс по tkinter
- ...
