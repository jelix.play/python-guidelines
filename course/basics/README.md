# Основы Python

## Index

[Подготовка](./preparation.md)

[Ввод-вывод](./io.md)

[Выражения и типы данных](./expressions_and_types.md)

[Переменные](./variables.md)

[Условия](./conditions.md)

[Простые встроенные возможности](./builtins_basic.md)

[Циклы](./loops)
