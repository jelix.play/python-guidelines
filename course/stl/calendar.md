# Модуль `calendar`

Модуль calendar позволяет напечатать себе календарик (а также содержит
некоторые другие полезные функции для работы с календарями).

## Основные функции

```python
calendar.calndar(2022)
```

Эта функция напечатает нам каленарик, в аргумент передаем год.

### calendar.setfirstweekday()

Функция `calendar.setfirstweekday()` устанавливает день недели 0 - понедельник,
6 - воскресенье, с которого начинается неделя.
Значения `calendar.MONDAY`, `calendar.TUESDAY`, `calendar.WEDNESDAY`, `calendar.THURSDAY`,
 `calendar.FRIDAY`, `calendar.SATURDAY` и `calendar.SUNDAY` приведены для удобства.

```python
calendar.setfirstweekday(6)  # эквивалентно calendar.setfirstweekday(calendar.SUNDAY)
```

### calendar.firstweekday()

Функция `calendar.firstweekday()` возвращает целое число, означающее день недели,
установленное в качестве начала недели.

```python
calendar.firstweekday()  # 0
```

### calendar.isleap(year)

Функция calendar.isleap() возвращает `True`, если год `year` является високосным,
в противном случае `False`.

```python
calendar.isleap(2024)  # True
calendar.isleap(2022)  # False
```

### calendar.leapdays(y1, y2)

Функция `calendar.leapdays()` возвращает количество високосных лет в диапазоне от
`y1` до `y2` (исключая), где `y1` и `y2` - годы.

```python
calendar.leapdays(2020, 2050)  # 8
```

Эта функция работает для диапазонов, охватывающих столетнюю эпоху.

### calendar.monthrange(year, month)

Функция `calendar.monthrange()` возвращает двойной [кортеж](https://gitlab.com/v01d-itcube/python-guidelines/-/blob/main/course/containers/tuple.md)
c первым рабочем днем месяца и количество дней в месяце для указанного года `year` и месяца `month`.

```python
calendar.monthrange(2020, 5)  # (4, 31)
calendar.monthrange(2020, 7)  # (2, 31)
```

Я перечислил только самые важные функции, а остальные вы можете посмотреть
[тут](https://docs.python.org/3/library/calendar.html).
