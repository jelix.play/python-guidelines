# Модуль `pprint`

автор: Nikase

Модуль `pprint` позволяет красиво отображать объекты `Python`.

## Функции

### `pprint.pprint()`

**pprint.pprint()** - отображает красиво значение нашего объекта.

*Давайте посмотрим, как же она отобразит значение нашей переменной в консоли?*

Например:

```python
my_question = {
    "question": {
        "question1": "Что такое модуль pprint",
        "question2": "Какие Классы имеет модуль pprint?",
        "question3": "Как может помочь мне этот модуль?",
    }
}

print(my_question)
```

```
{'question': {'question1': 'Что такое модуль pprint', 'question2': 'Какие Классы имеет модуль pprint?', 'question3': 'Как может помочь мне этот модуль?'}}
```

С модулем:

```python
my_question = {
    "question": {
        "question1": "Что такое модуль pprint",
        "question2": "Какие Классы имеет модуль pprint?",
        "question3": "Как может помочь мне этот модуль?",
    }
}


pprint.pprint(my_question)
```

```
{'question': {'question1': 'Что такое модуль pprint',
              'question2': 'Какие Классы имеет модуль pprint?',
              'question3': 'Как может помочь мне этот модуль?'}}
```

Как мы видим, каждый вопрос он отделил `\n` друг от друга.

### `pprint.pformat()`

**pprint.pformat()** - такой же, как **pprint.pprint()**, но уже принимает 2 аргумент. 1 аргумент принимает объект, а 2 аргумент принимает ширину нашего оформления.

Пример:

```python
my_question = {
    "question": {
        "question1": "Что такое модуль pprint",
        "question2": "Какие Классы имеет модуль pprint?",
        "question3": "Как может помочь мне этот модуль?",
    }
}


pprint.pformat(my_question, width=10)
```

```
{'question': {'question1': 'Что '
                           'такое '
                           'модуль '
                           'pprint',
              'question2': 'Какие '
                           'Классы '
                           'имеет '
                           'модуль '
                           'pprint?',
              'question3': 'Как '
                           'может '
                           'помочь '
                           'мне '
                           'этот '
                           'модуль?'}}
```

Вот так функция `pformat` выводит значение нашей переменной.

## Класс

Этот модуль определяет только один класс `PrettyPrinter`.

### `pprint.PrettyPrinter()`

**pprint.PrettyPrinter()** - это класс, конструктор которого принимает несколько именованных аргументов.

Примеры:

#### Аргумент `indent`

```python
my_question = {
    "question": {
        "question1": "Что такое модуль pprint",
        "question2": "Какие Классы имеет модуль pprint?",
        "question3": "Как может помочь мне этот модуль?",
    }
}

pretty = pprint.PrettyPrinter(indent=4)

pretty.pprint(my_question)
```

```
{   'question': {   'question1': 'Что такое модуль pprint',
                    'question2': 'Какие Классы имеет модуль pprint?',
                    'question3': 'Как может помочь мне этот модуль?'}}
```

#### Аргумент `width`

```python
my_question = {
    "question": {
        "question1": "Что такое модуль pprint",
        "question2": "Какие Классы имеет модуль pprint?",
        "question3": "Как может помочь мне этот модуль?",
    }
}

pretty = pprint.PrettyPrinter(width=41)

pretty.pprint(my_question)
```

```
{'question': {'question1': 'Что такое '
                           'модуль '
                           'pprint',
              'question2': 'Какие '
                           'Классы '
                           'имеет '
                           'модуль '
                           'pprint?',
              'question3': 'Как может '
                           'помочь мне '
                           'этот '
                           'модуль?'}}
```

#### Аргумент `depth`

```python
my_question = {
    "question": {
        "question1": "Что такое модуль pprint",
        "question2": "Какие Классы имеет модуль pprint?",
        "question3": "Как может помочь мне этот модуль?",
    }
}

pretty = pprint.PrettyPrinter(depth=1)

pretty.pprint(my_question)
```

```
{'question': {...}}
```

**Все аргументы класса `PrettyPrinter`:**

|      Аргумент       |                            Его значение                                 |
|---------------------|-------------------------------------------------------------------------|
|`indent`             | Количество Отступов.                                                    |
|`width`              | Ширина значение переменной.                                             |
|`depth`              | Глубина нашего значения.                                                |
|`stream`             | файлоподобный объект на который будет записан `write()`.                |
|`compact`            | Сделать значение переменной более компактным.                           |
|`sort_dicts`         | Сортировка, что и делает модуль pprint.                                 |
|`underscore_numbers` | Разделяет числа "_" тысячи миллионы и т.д. Например 10000 станет 10_000.|

Подробнее про `PrettyPrinter` можете прочитать [здесь](https://docs.python.org/3/library/pprint.html?highlight=pprint#pprint.PrettyPrinter).
