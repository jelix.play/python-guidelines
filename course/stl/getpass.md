# Модуль `getpass` 

*Автор: Vlad7777*

## `getpass.getpass`
принимает два аргумента: prompt (по умолчанию Password:) и stream (Игнорируется в Windows!!!).
Используется для запроса пароля. При вводе пароль не отображается.
Возвращает строку с паролем.

```python
import getpass


print(getpass.getpass())
```

вывод в консоль (был введён пароль 1029384756)

```
Password:
1029384756
```

## `getpass.getuser`
Не принимает аргументов. Используется для получения имени пользователя. Возвращает строку с именем пользователя.

```python
import getpass


print(getpass.getuser())
```

вывод в консоль (у меня пользовотеля зовут python)

```
python
```

на этом всё
