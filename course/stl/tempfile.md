# Tempfile

Модуль Tempfile позволяет создавать временные файлы.

## Создание временных файлов и каталогов

В качестве общей меры безопасности Python автоматически удаляет все временные данные после закрытия питона.
Давайте посмотрим на простой пример.

```py
import tempfile
​
# Мы создаем временный файл, используя tempfile.TemporaryFile().
temp = tempfile.TemporaryFile()
​
# Здесь хранятся временные файлы
temp_dir = tempfile.gettempdir()
​
print(f"Временный файл хранится в {temp_dir}")
​
print(f"Создал объект временного файла: {temp}")
print(f"Имя временного файла: {temp.name}")
​
# Удаление файла
temp.close()
```

Выход

```
Временный файл хранится в /tmp
Создал объект временного файла: <_io.BufferedRandom name=3>
Имя временного файла: 3
```

Давайте теперь попробуем найти этот файл, используя tempfile.gettemppdir(), чтобы получить директорию, где хранится папка Temp.

После запуска программы, если вы перейдете к temp_dir, вы можете увидеть, что созданного файла "3" там нету.

```
ls: cannot access '3': No such file or directory
```

Это доказывает, что Python автоматически удаляет эти временные файлы после закрытия программы.
Теперь, подобно созданию временных файлов, мы также можем создавать временные директории с использованием функции tempfile.temporarydirectory().

```py
tempfile.TemporaryDirectory(suffix=None, prefix=None, dir=None)
```

Для удобства и для красоты кода мы можем использовать менеджер контекста.

```py
import tempfile

with tempfile.TemporaryDirectory() as tmpdir:
    print(f"Created a temporary directory: {tmpdir}")

print("The temporary directory is deleted")
```

Вывод

```
Created a temporary directory: /tmp/tmpa3udfwu6
The temporary directory is deleted
```
