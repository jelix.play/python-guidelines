# Модуль `Math`

автор: AzRee

Python библиотека math содержит наиболее применяемые математические функции
и константы. Все вычисления происходят на множестве вещественных чисел.

## Основные констансты модуля `Math`

### math.pi

Представление математической константы π = 3.141592…. "Пи" — это отношение
длины окружности к её диаметру.

```python
print(math.pi)  # 3.141592653589793
```

### math.e

Число Эйлера или просто e. Иррациональное число, которое приблизительно
равно 2,71828.

```python
print(math.e)  # 2.718281828459045
```

### math.inf

положительная бесконечность.

```python
print(math.inf)  # inf
```

Хочу заметить, что `math.inf` = `float("inf")`

## Список основных функций

### math.ceil()

Функция округляет аргумент до большего целого числа.

```python
print(math.ceil(3.0001))  # 4
```

### math.factorial

Вычисление факториала. Входящее значение должно быть целочисленным и
неотрицательным.

```python
print(math.factorial(5))  # 120
```

### math.floor()

Антагонист функции `ceil()`. Округляет число до ближайшего целого, но в
меньшую сторону.

```python
print(math.floor(3.99))  # 3
```

### math.gcd(a, b)

Возвращает наибольший общий делитель `a` и `b`. НОД — это самое большое
число, на которое `a` и `b` делятся без остатка.

```python
import math


a = 5
b = 15
print(math.gcd(a, b))  # 5
```

### math.log()

Функция работает, как с одним, так и с двумя параметрами.

1 аргумент: вернёт значение натурального логарифма (основание `e`):

```python
print(math.log(math.e))  # 1.0
```

2 аргумента: вернёт значение логарифма по основанию, заданному во
втором аргументе:

```python
print(math.log(16, 4))  # 2.0
```

!!!Помните, это читается, как простой вопрос: "в какую степень
нужно возвести число `4`, чтобы получить `16`". Ответ, очевидно, `2`.
Функция `log()` с нами согласна.

### math.sqrt()

Возврат квадратного корня из аргумента.

```python
print(math.sqrt(16))  # 4.0
```

## Тригонометрические функции

### math.sin()

Функция вернёт синус угла. Угол следует задавать в радианах:

```python
print(math.sin(0))  # 0.0
```

### math.tan()

Тангенс угла. Аргумент указываем в радианах.

```python
print(math.tan(math.radians(315)))  # -1.0000000000000004
```

### math.cos()

Косинус угла, который следует указывать в радианах:

```python
print(math.cos(math.pi))  # -1.0
```

### math.degrees()

Функция переводит радианное значение угла в градусы.

```python
print(math.degrees(math.pi))  # 180
```

### math.radians()

Наоборот: из градусов — в радианы.

```python
print(math.radians(30))  # 0.5235987755982988
```

Я перечислил только самые важные функции, а остальные вы можете посмотреть
[тут](https://docs.python.org/3/library/math.html).
